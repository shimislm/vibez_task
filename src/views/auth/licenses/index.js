/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

// Chakra imports
import { Flex, Link, Text, Button, useColorModeValue } from "@chakra-ui/react";
import Card from "components/card/Card.js";
import { HSeparator } from "components/separator/Separator";
// Assets
import React, { useState } from "react";
import { Document, Page, pdfjs } from "react-pdf/dist/esm/entry.webpack";
import PDFTerms from "./terms.pdf";
import PDFPolicy from "./policy.pdf";

function Licenses() {
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);
  const [PDF, setPDF] = useState("policy");
  var url = PDF.base;

  function removeTextLayerOffset() {
    const textLayers = document.querySelectorAll(
      "div.react-pdf__Page__annotations, div.react-pdf__Page__textContent"
    );
    textLayers.forEach((el) => el.remove());
  }

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
    removeTextLayerOffset();
  }

  return (
    <Flex
      flexDirection={"column"}
      alignItems="center"
      justifyContent={"center"}
    >
      <Card
        width="800px"
        minW="350px"
        m="20px"
        brandColor="navy"
        borderWidth="1px"
      >
        <Flex
          flexDirection="row"
          m="20px"
          justifyContent="center"
          alignItems="center"
        >
          <Button
            m="10px"
            onClick={() => setPDF("policy")}
            variant={PDF == "policy" ? "action" : "light"}
            size="lg"
            fontSize="md"
            borderRadius="70px"
            px="24px"
            py="5px"
          >
            Private Policy
          </Button>
          <Button
            m="10px"
            onClick={() => setPDF("terms")}
            variant={PDF == "terms" ? "action" : "light"}
            size="lg"
            fontSize="md"
            borderRadius="70px"
            px="24px"
            py="5px"
          >
            Terms of Service
          </Button>
        </Flex>
        <Document
          file={PDF == "policy" ? PDFPolicy : PDFTerms}
          onLoadSuccess={onDocumentLoadSuccess}
        >
          {Array.from(new Array(numPages), (el, index) => (
            <>
              <Page
                key={`page_${index + 1}`}
                pageNumber={index + 1}
                onLoadSuccess={removeTextLayerOffset}
              />
              <HSeparator mb="20px" mt="20px" />
            </>
          ))}
          {/* <Page height="100%" pageNumber={pageNumber} color="black" /> */}
        </Document>
      </Card>
    </Flex>
  );
}

export default Licenses;
