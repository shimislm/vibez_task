import React from "react";

// Chakra imports
import { Box, Flex, Text } from "@chakra-ui/react";

function SuccessMsg() {
  // Chakra color mode
  const textColorSecondary = "gray.400";
  return (
    <Flex
      w="100%"
      maxW="max-content"
      mx={{ base: "auto", lg: "0px" }}
      me="auto"
      h="100%"
      alignItems="start"
      justifyContent="center"
      // mb={{ base: "30px", md: "60px", lg: "100px", xl: "60px" }}
      px={{ base: "25px", md: "0px" }}
      // mt={{ base: "40px", lg: "16vh", xl: "22vh" }}
      flexDirection="column"
    >
      <Box me="auto">
        <Text
          ms="4px"
          color={textColorSecondary}
          fontWeight="400"
          fontSize="md"
        >
          We sent you an email.
        </Text>
        <Text
          ms="4px"
          color={textColorSecondary}
          fontWeight="900"
          fontSize="md"
        >
          Please verify your account by pressing the link in the email.
        </Text>
      </Box>
    </Flex>
  );
}

export default SuccessMsg;
