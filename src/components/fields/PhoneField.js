// import "react-phone-number-input/style.css";
import PhoneInput from "react-phone-number-input";
// Chakra imports
import {
  Flex,
  FormLabel,
  Input,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import "./PhoneFieldStyle.css";

export default function PhoneField({ value, setValue, mb, h }) {
  // `value` will be the parsed phone number in E.164 format.
  // Example: "+12133734253".

  const textColorPrimary = useColorModeValue("secondaryGray.900", "white");
  const bgPrimary = useColorModeValue("transparent", "navy.800");
  const borderPrimary = useColorModeValue(
    "secondaryGray.100",
    "whiteAlpha.100"
  );

  return (
    <Flex
      h={h ? h : "48px"}
      w="100%"
      pr="16px"
      pl="16px"
      justifyContent={"center"}
      direction="column"
      mb={mb ? mb : "30px"}
      bg={bgPrimary}
      border="1px solid "
      borderColor={borderPrimary}
      borderRadius="16px"
    >
      <PhoneInput
        defaultCountry="IL"
        international
        value={value}
        onChange={setValue}
        // _placeholder={{ color: "secondaryGray.500" }}
      />
    </Flex>
  );
}
