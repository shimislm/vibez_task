import React from "react";

// Chakra imports
import { Button } from "@chakra-ui/react";

// Custom components
import { AddIcon } from "@chakra-ui/icons";

export function AddButton({ text, action }) {
  // Chakra Color Mode
  return (
    <Button
      onClick={action}
      leftIcon={<AddIcon />}
      size="lg"
      variant="brand"
      fontSize="md"
      fontWeight="500"
      borderRadius="70px"
      px="24px"
      py="5px"
    >
      {text}
    </Button>
  );
}
